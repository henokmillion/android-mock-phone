package com.sync.in.mock;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by User on 5/11/2018.
 */

public class FavRecyclerViewAdapter extends RecyclerView.Adapter<FavRecyclerViewAdapter.FavViewHolder> {

    private Context mContext;
    private List<Contact> mData;

    public FavRecyclerViewAdapter(Context mContext, List<Contact> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public FavViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_fav, parent, false);
        FavRecyclerViewAdapter.FavViewHolder vHolder = new FavRecyclerViewAdapter.FavViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(FavViewHolder holder, int position) {
        holder.fav_letter.setText(mData.get(position).getLetter());
        holder.fav_name.setText(mData.get(position).getName());
        holder.fav_phone.setText(mData.get(position).getPhone());
//        Toast.makeText(mContext, String.valueOf(mData.get(position).getPhoto()), Toast.LENGTH_LONG).show();

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    static class FavViewHolder extends RecyclerView.ViewHolder {

        private TextView fav_name, fav_phone, fav_letter;
        private LinearLayout bg;

        public FavViewHolder(View itemView) {
            super(itemView);
            bg = (LinearLayout) itemView.findViewById(R.id.fav_item_layout);
            fav_name = (TextView) itemView.findViewById(R.id.fav_name);
            fav_letter = (TextView) itemView.findViewById(R.id.fav_letter);
            fav_phone = (TextView) itemView.findViewById(R.id.fav_phone);


        }
    }

}
