package com.sync.in.mock;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/6/2018.
 */

public class FragmentFav extends Fragment {

    View v;
    private RecyclerView mRecyclerView;
    private List<Contact> lstContacts;


    public FragmentFav() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstContacts = new ArrayList<>();
        lstContacts.add(new Contact("Bill", "0903123456", R.color.colorPrimaryDark, "B"));
        lstContacts.add(new Contact("Averill", "0903123456", R.color.colorPrimary, "A"));
        lstContacts.add(new Contact("Dani", "0903123456", R.color.colorPrimaryDark, "D"));
        lstContacts.add(new Contact("Bill", "0903123456", R.color.colorSecondaryDark, "B"));
        lstContacts.add(new Contact("Averill", "0903123456", R.color.colorPrimary, "A"));
        lstContacts.add(new Contact("Dani", "0903123456", R.color.colorPrimaryDark, "D"));
        lstContacts.add(new Contact("Bill", "0903123456", R.color.colorSecondaryDark, "B"));
        lstContacts.add(new Contact("Averill", "0903123456", R.color.colorPrimary, "A"));
        lstContacts.add(new Contact("Dani", "0903123456", R.color.colorPrimaryDark, "D"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fav_fragment, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.fav_recyclerview);
        FavRecyclerViewAdapter favRecyclerViewAdapter = new FavRecyclerViewAdapter(getContext(), lstContacts);
        mRecyclerView.setAdapter(favRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return v;
    }
}
