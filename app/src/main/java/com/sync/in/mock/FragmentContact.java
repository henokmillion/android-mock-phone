package com.sync.in.mock;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/6/2018.
 */

public class FragmentContact extends Fragment {
    View v;
    private RecyclerView mRecyclerView;
    private List<Contact> lstContacts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lstContacts = new ArrayList<>();
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
        lstContacts.add(new Contact("Bill Gates", "0911000000", R.drawable.face1));
    }

    public FragmentContact() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.contact_fragment, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.contact_recyclerview);
        RecyclerViewAdapter mRecyclerViewAdapter = new RecyclerViewAdapter(getContext(), lstContacts);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return v;
    }
}
