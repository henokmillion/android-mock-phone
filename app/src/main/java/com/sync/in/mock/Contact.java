package com.sync.in.mock;

/**
 * Created by User on 5/10/2018.
 */

public class Contact {

    private String name;
    private String phone;
    private String letter;
    private int Photo;

    public Contact(String name, String phone, int photo) {
        this.name = name;
        this.phone = phone;
        Photo = photo;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public Contact(String name, String phone, int photo, String letter)
    {
        this.name = name;
        this.phone = phone;
        this.Photo = photo;
        this.letter = letter;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getPhoto() {
        return Photo;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPhoto(int photo) {
        Photo = photo;
    }

}
